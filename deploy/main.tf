terraform {
  backend "s3" {
    bucket         = "recipe-terraform-state-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "recipe-terraform-state-lock"
  }
}

provider "aws" {
  region  = "eu-central-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
